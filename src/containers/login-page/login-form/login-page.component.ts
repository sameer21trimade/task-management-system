import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { User } from '../user-type/user';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class LoginPageComponent implements OnInit {
  userModel: any = new User('', '');
  public isError = false;
  public authenticationError = false;
  public userAuthentication: any = {};
  public userInfoArray = [];
  public submitButton = {
    name: 'SIGN IN',
    selectBtn: 'primary',
    type: 'submit',
  };

  constructor(private router: Router) {}

  ngOnInit(): void {
    if (localStorage.getItem('userInfo'))
      this.userInfoArray = JSON.parse(localStorage.getItem('userInfo'));
  }
  onSubmit() {
    if (
      !this.userModel.name ||
      !this.userModel.password ||
      this.userModel.name.length < 4 ||
      this.userModel.password.length < 8
    )
      this.isError = true;
    else if (!this.userInfoArray.length) {
      this.userInfoArray.push(this.userModel);
      localStorage.setItem('userInfo', JSON.stringify(this.userInfoArray));
      this.router.navigateByUrl('/homepage');
    } else {
      let newUser = true;
      let flag = true;
      this.userInfoArray.forEach((index) => {
        if (
          index.name === this.userModel.name &&
          index.password !== this.userModel.password
        ) {
          this.authenticationError = true;
          newUser = false;
        } else if (
          index.name === this.userModel.name &&
          index.password === this.userModel.password
        ) {
          this.router.navigateByUrl('/homepage');
          flag = false;
        }
      });
      if (newUser && flag) {
        this.userInfoArray.push(this.userModel);
        localStorage.setItem('userInfo', JSON.stringify(this.userInfoArray));
        this.router.navigateByUrl('/homepage');
      }
    }
  }
}
