export interface json {
  userId: number;
  id: number;
  title: string;
  body: string;
}
