import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { catchError } from 'rxjs/operators';
import { throwError as observableThrowError, Observable } from 'rxjs';
import { json } from './json.data';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class TaskManagementService {
  
  constructor(private http: HttpClient) {}
  private url: string = 'https://jsonplaceholder.typicode.com/posts/';
  private newTask = new BehaviorSubject([]);
  currentTasksList = this.newTask.asObservable();

  createTask(data: any) {
    this.newTask.next(data);
  }

  getTasks(): Observable<json[]> {
    return this.http.get<json[]>(this.url).pipe(catchError(this.errorHandler));
  }

  errorHandler(error: HttpErrorResponse) {
    return observableThrowError(error.message || 'Internal Server Error');
  }
}
