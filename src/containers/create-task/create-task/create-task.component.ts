import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { NewTask } from '../new-task-type/new-task';
import { Router } from '@angular/router';
import { TaskManagementService } from '../../service/task-management.service';

@Component({
  selector: 'app-create-task',
  templateUrl: './create-task.component.html',
  styleUrls: ['./create-task.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class CreateTaskComponent implements OnInit {
  newTask = new NewTask('', '');
  public isError = false;
  shareNewTask: any;
  public createTaskButton = {
    name: 'CREATE TASK',
    selectBtn: 'primary',
    type: 'submit',
  };

  constructor(
    private router: Router,
    private storeNewTask: TaskManagementService
  ) {}

  ngOnInit(): void {
    this.storeNewTask.currentTasksList.subscribe(
      (data) => (this.shareNewTask = data)
    );
  }

  onSubmit() {
    if (!this.newTask.title || !this.newTask.body)
      this.isError = true;
    else {
      this.storeNewTask.createTask({
        userId: Math.random(),
        id: Math.random(),
        title: this.newTask.title,
        body: this.newTask.body,
        status: 'Open',
      });
      this.router.navigateByUrl('/homepage');
    }
  }
}
