export class NewTask {
    constructor(
        public title: string,
        public body: string,
    ) {}
}