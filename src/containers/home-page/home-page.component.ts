import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { TaskManagementService } from '../service/task-management.service';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class HomePageComponent implements OnInit {
  public nonFilteredData = [];
  public errorMsg = '';
  public title = 'Get things done.';
  public filteredData = [];
  public searchInput = '';
  public status = ['Open', 'In Progress', 'Done'];
  public sharedNewTask: any;
  public emptyState = false;
  public createTaskButton = {
    name: '+ CREATE TASK',
    selectBtn: 'secondary',
    type: 'button',
  };

  constructor(
    private _taskManagementService: TaskManagementService,
    private storeNewTask: TaskManagementService
  ) {}

  ngOnInit(): void {
    this._taskManagementService.getTasks().subscribe(
      (data) => {
        data.forEach((index) => {
          index['status'] = 'Open';
        });
        if (this.sharedNewTask.title) {
          data.unshift(this.sharedNewTask);
        }
        this.nonFilteredData = data;
        this.filteredData = data;
      },
      (error) => (this.errorMsg = error)
    );
    this.storeNewTask.currentTasksList.subscribe((data) => {
      this.sharedNewTask = data;
    });
  }

  deleteItem(id) {
    this.filteredData = this.filteredData.filter((index) => index.id !== id);
    this.nonFilteredData = this.filteredData;
  }

  searchTasks(event) {
    this.filteredData = this.nonFilteredData;
    this.filteredData = this.filteredData.filter((index) =>
      index.title.toLowerCase().includes(event.toLowerCase())
    );
    this.emptyState = false;
    if (!this.filteredData.length) this.emptyState = true;
  }

  setStatus(value, id) {
    this.filteredData.forEach((index) => {
      if (index.id === id) {
        index['status'] = value;
      }
    });
    this.nonFilteredData = this.filteredData;
  }
  
  checkStatus(value) {
    this.filteredData = this.nonFilteredData;
    this.filteredData = this.filteredData.filter(
      (index) => index.status === value
    );
    this.emptyState = false;
    if (!this.filteredData.length) this.emptyState = true;
  }
}
