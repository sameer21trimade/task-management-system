import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomePageComponent } from '../home-page/home-page.component';
import { CreateTaskComponent } from '../create-task/create-task/create-task.component';
import { LoginPageComponent } from '../login-page/login-form/login-page.component';

const routes: Routes = [
  { path: '', redirectTo: '/login', pathMatch: 'full' },
  { path: 'homepage', component: HomePageComponent },
  { path: 'createtask', component: CreateTaskComponent },
  { path: 'login', component: LoginPageComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}

export const RoutingComponents = [HomePageComponent, CreateTaskComponent];
