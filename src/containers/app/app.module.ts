import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule, RoutingComponents } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomePageComponent } from '../home-page/home-page.component';
import { FormsModule } from '@angular/forms';
import { CreateTaskComponent } from '../create-task/create-task/create-task.component';
import { LoginPageComponent } from '../login-page/login-form/login-page.component';
import { ButtonComponent } from '../../components/button/button.component';

@NgModule({
  declarations: [
    AppComponent,
    HomePageComponent,
    CreateTaskComponent,
    LoginPageComponent,
    ButtonComponent,
  ],
  imports: [BrowserModule, AppRoutingModule, HttpClientModule, FormsModule],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
